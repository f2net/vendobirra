-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Mar 29, 2016 alle 18:59
-- Versione del server: 5.5.47-0ubuntu0.14.04.1
-- Versione PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vendobirra`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `birra`
--

CREATE TABLE IF NOT EXISTS `birra` (
  `birra_id` int(11) NOT NULL AUTO_INCREMENT,
  `birrificio_id` int(11) NOT NULL,
  `stile_id` int(11) DEFAULT NULL,
  `imageUrl` varchar(200) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `stile` varchar(100) NOT NULL,
  `descrizione` varchar(4000) NOT NULL,
  `grado_alcolico` decimal(3,1) NOT NULL,
  `ibu` int(11) NOT NULL,
  `prezzo` decimal(10,2) NOT NULL,
  `malti` varchar(500) NOT NULL,
  `luppoli` varchar(500) NOT NULL,
  `totalRating` decimal(10,2) NOT NULL,
  `ratingsNumber` int(11) NOT NULL,
  PRIMARY KEY (`birra_id`),
  KEY `birrificio_id` (`birrificio_id`),
  KEY `stile_id` (`stile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dump dei dati per la tabella `birra`
--

INSERT INTO `birra` (`birra_id`, `birrificio_id`, `stile_id`, `imageUrl`, `nome`, `stile`, `descrizione`, `grado_alcolico`, `ibu`, `prezzo`, `malti`, `luppoli`, `totalRating`, `ratingsNumber`) VALUES
(1, 1, 1, 'elav_celtic_mater.jpg', 'Celtic Mater', 'Pale Ale', 'La Celtic Mater, alla vista di color giallo carico tendente al dorato e lievemente velato, ha una schiuma bianca, di trama fina e buona persistenza. \r\nIl luppolo Saaz sprigiona forti note floreali, erbacee e leggermente speziate e il malto utilizzato contribuisce a questo aroma con lievi sentori di crosta di pane.\r\nAl gusto domina il malto con delicate note di miele e di cereale che conducono la bevuta verso un finale secco, dall’amaro delicato ed erbaceo che mantiene una impressionante pulizia del palato.', 4.5, 25, 5.00, 'Pale, Pils', 'Saaz', 1718.96, 562),
(2, 1, 2, 'elav_indie_ale.jpg', 'Indie Ale', 'Amber Ale', 'La Indie Ale è caratterizzata da un color ambrato, limpido e da una schiuma pannosa molto compatta. Presenta un sorprende bilanciamento tra l’abbondante luppolatura e l’utilizzo dei malti.\r\nLe note agrumate, tropicali dei luppoli utilizzati convivono in perfetta armonia con le più dolci sfumature donate dai malti e con le sensazioni di caramello che lentamente si fondono con quelle fruttate, tropicali e agrumate apportate dall’abbondante luppolatura.\r\nIl finale rivela a sorpresa un amaro che tende all’erbaceo, molto delicato e per nulla invadente, invita anzi il bevitore a poggiare immediatamente le labbra al bicchiere, per godere di un altro sorso.', 5.5, 40, 5.00, 'Pils, Monaco Scuro, Crystal', 'Columbus, Sorachi, Cascade, Citra, Centennial, Simcoe, Willamette', 3758.40, 1044),
(3, 1, 3, 'elav_grunge_ipa.jpg', 'Grunge IPA', 'American Ipa', 'La Grunge Ipa si presenta di un bel colore ambrato carico, tendente al rosso. La schiuma che si forma è bianca, compatta e di buona persistenza.\r\nAccattivante e ruffiana allo stesso tempo, sprigiona nel medesimo istante, ma con ottimo equilibrio, dolci fragranze maltate e forti aromi luppolati con netti richiami di frutta tropicale e rinfrescanti note resinose, erbacee e di aghi di pino.\r\nAl gusto prevale inizialmente il profilo maltato che, dolce e caramellato, conduce verso quella che è il marchio di fabbrica di questa birra: la luppolatura.', 6.3, 60, 5.00, 'Pale, Crystal', 'Columbus, Cascade, Centennial, Amarillo', 5891.80, 1655),
(4, 1, 4, 'elav_punks_do_it_bitter.jpg', 'Punks Do It Bitter', 'Best Bitter', 'La Punks Do It Bitter è una birra dal colore dorato chiaro leggermente velato con una schiuma pannosa e di buona tenuta.\r\nSprigiona inebrianti aromi agrumati, con una netta predominanza di pompelmo, che rendono già all’olfatto, e poi al gusto, l’idea di quanto questa birra possa essere fresca e dissetante. \r\nHa un corpo leggero, scorre veloce ma non frettolosa verso un finale secco dall’amaro delicato e persistente.\r\nMarchio di fabbrica della Punks Do It Bitter è il consistente Dry Hopping, con Mosaic e Cascade, che le dona la caratteristica agrumata evidente in ogni fase della degustazione.', 4.3, 35, 5.00, 'Pale, Monaco Chiaro, Crystal', 'Palisade, Cascade, Columbus, Mosaic', 4115.36, 1156),
(5, 1, 5, 'elav_dark_metal.jpg', 'Dark Metal', 'Imperial Stout', 'La Dark Metal si presenta di color nero impenetrabile, la sua schiuma, compatta e di buona persistenza, è beige intenso, quasi marrone. \r\nGli aromi sprigionati derivano principalmente dai malti tostati, con forti richiami di caffè e cioccolato.\r\nA completare il profilo aromatico sono i profumi balsamici e resinosi regalati dal luppolo Chinook che conducono la bevuta ad un finale piacevolemente amaro, dove il balsamico del luppolo ricorda la liquirizia.', 8.5, 90, 6.00, 'Pale, Monaco Chiaro, Crystal, Chocolate, Black, Roasted Barley', 'Chinook', 70602.33, 1593),
(6, 1, 6, 'elav_uppercut_ipa.jpg', 'Uppercut IPA', 'Ipa', 'La Uppercut Ipa, dal colore ambrato scuro, possiede un aroma di dolci frutti rossi e rosmarino. Per aumentarne l’eleganza infatti, nella fase finale di bollitura viene aggiunto del rosmarino che le dona un sentore leggermente balsamico che ben si lega con i profumi dei luppoli americani che la caratterizzano.', 6.5, 30, 5.00, 'Pale, Crystal Scuro, Monaco Scuro', 'Chinook, Willamette, Mosaic', 1387.44, 434),
(7, 1, 7, 'elav_techno_double_ipa.jpg', 'Techno Double IPA', 'Imperial Ipa', 'La Techno Double Ipa si presenta di colore ambrato intenso con una schiuma bianca compatta. I luppoli utilizzati, Simcoe e Chinook, le donano piacevoli note esotiche e fruttate di mango e pompelmo che invitano alla bevuta.\r\nQuesta birra possiede infatti, nonostante l’alta gradazione alcolica, una sorprendente bevibilità. Il singolo malto in loop come base e le alte frequenze di luppolo in bollitura e dry hopping, scatenano un rave party extrasensoriale.', 9.5, 95, 7.00, 'Pale', 'Chinook, Simcoe', 2323.68, 618),
(8, 1, 8, 'elav_humulus_black_ipa.jpg', 'Humulus Black IPA', 'Black Ipa', 'La Humulus è caratterizzata da un colore bruno intenso e da una schiuma corposa color cappuccino.\r\nI malti scuri poco tostati e l’abbondante luppolatura con il singolo luppolo americano (Cascade) le conferiscono quelle fresche note agrumate che ben si legano all’aroma di caffè e orzo tostato caratterizzando una persistenza gustativa elegante.\r\nA fine fermentazione inoltre, per accentuarne il profumo, viene aggiunto del luppolo anche a freddo.', 6.6, 34, 5.00, 'Pale, Monaco Scuro, Roasted Barley', 'Cascade', 933.96, 258),
(9, 1, 9, 'elav_sick_and_tired_scotch_ale.jpg', 'Sick and Tired Scotch Ale', 'Scotch Ale', 'La La Sick and Tired si presenta con un colore rosso rubino profondo ed è caratterizzata da una decisa nota caramellata e un leggero sentore torbato. \r\nSette le tipologie di malto utilizzate, tra cui un malto torbato tipico della produzione di Scotch Whisky.', 7.0, 30, 6.00, 'Pale, Crystal, Monaco Chiaro, Peated, Monaco Scuro, Crystal Scuro, Melano', 'Palisade', 796.95, 231),
(10, 1, 10, 'elav_regina_d_inverno.jpg', 'Regina d''Inverno', 'Porter', 'La Regina d’Inverno si presenta di colore nero con una bella schiuma beige intenso, quasi marrone.\r\nUna vera esplosione di aromi. Le note tostate di caffè e cioccolato, tipiche dello stile, vengono impreziosite da sensazioni di dolcezza conferite dall’uvetta e da note speziate e balsamiche apportate da zenzero e luppoli.\r\nIl corpo è leggero e l’attacco al palato apparentemente dolce, lascia spazio a ciò che una vera Porter dovrebbe manifestare. Caffè, cioccolato e liquirizia primeggiano senza però nascondere la speziatura originale dell’uvetta che le dona quel tocco in più di dolcezza e dello zenzero e scorza d’arancia che le regalano freschezza e, insieme ai luppoli, conducono la bevuta verso un finale piacevolmente amaro.\r\nLe caratteristiche aromatiche del luppolo giapponese Sorachi si mescolano ad un sorprendente aroma di caramello. Nonostante l’utilizzo di un singolo malto e di un singolo luppolo questa birra stupisce per la sua complessità e la sua spiccata personalità.', 7.0, 40, 5.00, 'Pale, Chocolate, Crystal Scuro, Black', 'Columbus, Ahtanum, Cascade', 504.60, 145),
(11, 1, 11, 'elav_progressive_barley_wine.jpg', 'Progressive Barley Wine', 'Barley Wine', 'La Progressive Barley Wine presenta un colore ambrato carico dato dalla lunga bollitura e una schiuma quasi inesistente, tipica dello stile a cui appartiene.\r\nLe caratteristiche aromatiche del luppolo giapponese Sorachi si mescolano ad un sorprendente aroma di caramello. Nonostante l’utilizzo di un singolo malto e di un singolo luppolo questa birra stupisce per la sua complessità e la sua spiccata personalità.\r\nLa Progressive Barley Wine, birra Elav con la più alta gradazione alcolica, necessita di una lunga maturazione, da un minimo di sei mesi ad un massimo di un anno.', 11.0, 65, 7.00, 'Pale, Pils', 'Sorachi', 845.17, 223),
(12, 2, 12, 'valcavallina_cavallina.jpg', 'Cavallina', 'Blonde Ale', 'Birra dal colore giallo paglierino, con una schiuma bianca, fine e persistente.\r\nAl naso malto leggero, seguito da aromi erbacei di fieno.\r\nIn bocca è fresca, parte dolce di malto per deviare rapidamente sull''erbaceo del luppolo.\r\nIl corpo e la leggera carbonatazione ne fanno una birra dissetante e beverina.\r\nOttima con antipasti, insalate, risotti, carni bianche, pesce arrosto e ai ferri.', 5.2, 0, 5.00, 'Pils', '', 2153.20, 458),
(13, 2, 13, 'valcavallina_sun_flower.jpg', 'Sunflower', 'Golden Ale', 'Birra di colore oro carico, schiuma a bolle fini, con buona compattezza, aderenza e persistenza.\r\nAl naso ha un bouquet molto gradevole che denota, accostato ad un miele leggerissimo, una buona componente agrumata, di frutta tropicale, mango e pesca.\r\nIn bocca è di corpo leggero, amara e aromatica da luppolo che porta ancora note agrumate, di frutta tropicale di luppolo in fiore ed erbacee con finale secco.\r\nSi abbina ad antipasti (salumi, pancetta...), primi piatti ricchi e saporiti (con carne o a tendenza dolce), formaggi di media stagionatura, carni bianche arrosto, uccelli con la panna.', 4.3, 32, 5.00, '', '', 1043.40, 282),
(14, 2, 14, 'valcavallina_polly_jean.jpg', 'Polly Jean', 'Light IPA', 'Birra di colore aranciato ,ha schiuma fine, compatta, aderente e con discreta persistenza.\r\nAl naso è intensa ed elegante, con aromi sia di tipo aggrumato che di frutta tropicale che floreali leggeri a coprire una leggera nota maltata con richiami mielati.\r\nIn bocca ha una frizzantezza molto contenuta, accostata a un corpo esile e un’amarezza ben presente ma mai invadente. Si può avvertire inizialmente il malto, contrastato poi dall’amaro del luppolo, mentre l’aromaticità è data dallo stesso fruttato avvertito al naso con in più una componente erbacea che ci porta in un finale secco e asciutto.\r\nSi abbina bene ad  antipasti a tendenza sapida, tempura di verdure, primi con verdure o frutti di mare, carne bianca o pesce grasso alla griglia e formaggi di media stagionatura.', 4.3, 0, 5.00, '', '', 543.60, 151),
(15, 2, 15, 'valcavallina_calypso.jpg', 'Calypso', 'American Pale Ale', 'Birra dal colore ambrato con riflessi aranciati, schiuma bianca, fine, di buona persistenza.\r\nNaso intenso, equilibrato tra le note dei malti e quelle dei luppoli americani con toni agrumati, di frutta tropicale e resinosi.\r\nAl palato è di facile beva, con toni del malto accompagnati da un agrumato di mandarino e pompelmo che donano un finale secco e pulito, un amaro non eccessivo ma appagante.\r\nSi abbina a primi speziati, salmone, ottima per pasteggiare.', 5.2, 38, 5.00, '', '', 697.08, 200),
(16, 2, 16, 'valcavallina_vecchia_volpe.jpg', 'Vecchia Volpe', 'Brown Ale', 'Birra di colore ambrato pieno con una schiuma di grana fine, di buona compattezza ed aderenza.\r\nAl naso ha una buona intensità e  complessità con note maltate di caramello e leggero tostato che richiama la nocciola, seguite da un fruttato di frutta rossa e da una nota di burro di cacao.\r\nIn bocca è di frizzantezza moderata, corpo leggero, ci accoglie con un buon attacco maltato seguito da note ancora di frutta rossa e cacao nel finale. Arriva poi una buona componente dal luppolo che pulisce il cavo orale donando un finale secco e accompagnandoci anche nel retrogusto con note erbacee miste a quelle delle tostature.\r\nSi abbina a Bruschetta con patè di fegatini, Foie gras, carpacci di pesce o carne affumicati, pasta al sugo di noci, carne rossa alla griglia o con lunghe cotture, sbrisolona.', 5.0, 0, 5.00, '', '', 343.53, 99),
(17, 2, 17, 'valcavallina_alba_rossa.jpg', 'Alba Rossa', 'Extra Special Bitter', 'Birra dal colore bruno con riflessi ramati con cappello di schiuma bianca, piuttosto persistente.\r\nAl naso frutta matura, malto, sino al finale erbaceo di luppolo.\r\nIn bocca, corpo medio, con lievi toni di nocciola e frutta matura accompagnati dall''amaro erbaceo dei luppoli inglesi.\r\nIl finale lungo è predominato dall''amaro erbaceo seguito da note maltate.\r\nOttima con carni rosse, agnello, pasta al ragù, e con alcuni formaggi (parmigiano, grana).', 6.4, 48, 5.00, '', '', 551.01, 145),
(18, 2, 18, 'valcavallina_dark_side.jpg', 'Dark Side', 'Smoked Porter', 'Birra ispirata alle Smoked Porter britanniche.\r\nSi presenta di colore bruno scuro con riflessi rubini. Schiuma abbondante con bolle fini, compatta e cremosa con buona aderenza e persistenza.\r\nAl naso è intensa con buona complessità caratterizzata da una nota affumicata e tostata, seguita da note di caffè e cioccolato.\r\nIn bocca ha la giusta frizzantezza, corpo rotondo, ritroviamo note affumicate, di caffè e cioccolato/cacao. Il finale è piuttosto secco e pulito.\r\nSi abbina a salumi e pesci affumicati, carni brasate e alla griglia, formaggi molto stagionati, gelato alla vaniglia e fragole.', 6.0, 0, 6.00, '', '', 641.70, 165),
(19, 2, 11, 'valcavallina_diavolo.jpg', 'Diavolo', 'Barley Wine', 'Birra dal colore ambrato carico con schiuma a grana fine, aderente e persistente.\r\nNaso intrigante, pulito ed intenso, aromi di caramello e frutta rossa con richiami di marmellata e frutta secca.\r\nIn bocca è equilibrata tra la parte dolce e amara, corpo rotondo ma non stucchevole, con una buona percezione del luppolo e una calda sensazione alcolica ben gestita che ne esalta la bevibilità.\r\nRitroviamo decise note di caramello, frutta rossa e frutta secca.\r\nIn cucina si abbina a brasati, cacciagione, formaggi ben stagionati, cioccolato fondente e pasticceria secca.', 9.0, 0, 5.00, '', '', 163.20, 48),
(20, 3, 19, 'viapriula_loertis.jpg', 'Loertìs', 'Bohemian Pils', 'Loertìs in dialetto locale significa luppolo, ed è proprio questo elemento a caratterizzare questa fantastica birra artigianale di Via Priula. Una Birra in perfetto stile Pilsner boemo, caratterizzata dalle tipiche note erbacee e secche che rendono la birra leggera ed estremamente dissetante. Una birra ottima ogni giorno, ma perfetta per la stagione estiva, da sorseggiare magari all''ombra di un grande albero... eh sì, la Loertìs ci fa sognare!', 4.6, 33, 5.00, '', '', 381.60, 106),
(21, 3, 15, 'viapriula_bacio.jpg', 'Bacio', 'American Pale Ale', 'La Bacio è una birra artigianale prodotta ispirandosi ad un elisir dei primi anni 900, prodotto da un farmacista a San Pellegrino Terme. Questo elisir non esiste più ma rimane la targa pubblicitaria che definiva la Bacio una birra digestiva, soave e delicata... noi non sappiamo se cura il corpo, ma di sicuro cura lo spirito!', 4.9, 32, 5.00, '', '', 399.75, 173),
(22, 3, 20, 'viapriula_rosa.jpg', 'Rosa!', 'Speciale alla frutta (lamponi)', 'La Rosa! nasce da un''ispirazione di tratto sportivo. Infatti sebbene la ricetta esistesse già, mancava un''etichetta e un nome. Il Giro d''Italia che passò a San Pellegrino fu la chiave di volta! Rosa come la maglia dei vincitori e come il colore della birra, e sull''etichetta richiami al mondo del ciclismo. Sebbene sia leggera e dissetante non conviene che ne beviate troppa prima di montare in sella alla vostra bici!', 4.3, 13, 5.00, '', '', 273.75, 75),
(23, 3, 21, 'viapriula_corna_bianca.jpg', 'Corna Bianca', 'Blanche', 'Via Priula, come sempre, ha voluto rendere omaggio alla sua terra, dedicando la sua Blanche alla Corna Bianca, una maestosa falesia calcarea, nelle cui zone crescono la mentuccia e il ginepro usati per produrla. Il risultato è una birra fresca e dissetante, con la classica sensazione acidula ma che stupisce nel finale grazie alla nota mentolata che avvolge la bocca.', 5.0, 16, 5.00, '', '', 168.50, 50),
(24, 3, 22, 'viapriula_dubec.jpg', 'Dubec', 'Doppelbock', 'Dubec è la versione bergamasca della Doppelbock. Il nome nasce da un simpatico gioco di parole, infatti doppelbock in tedesco significa "due caproni" e Du Bec è la traduzione in dialetto bergamasco. L''etichetta raffigura due bellissimi esemplari maschi di Capra Orobica, giusto per rimarcare il legame con il territorio.', 7.5, 28, 5.00, '', '', 222.62, 63),
(25, 3, 23, 'viapriula_morosa.jpg', 'Morosa', 'Dubbel alla frutta (more)', 'Morosa, nel dialetto locale significa "fidanzata", ma non aspettatevi una brodaglia melensa! Anzi questa ragazza sa il fatto suo. Profumata, di carattere ma senza esagerare... le more le conferiscono una piacevole freschezza e sensazioni floreali. Una birra perfetta per ogni stagione!', 6.5, 20, 4.00, '', '', 13039.88, 1296),
(26, 3, 24, 'viapriula_melafoi.jpg', 'Mélafòi', 'Tripel con "Achillea Millefoglie"', 'Nella cultura birraria nordica l''achillea millefoglie era usata come alternativa al luppolo. Oggi Via Priula la riprende unendola ad una triple dal profilo speziato ed alcolico, ottenendo una nuova interpretazione di questo stile birrario.', 9.5, 37, 5.00, '', '', 100.92, 29),
(27, 3, 5, 'viapriula_camoz.jpg', 'Camoz', 'Imperial Stout', 'Camoz, in dialetto locale significa Camoscio, ed in effetti non c''è nome più azzeccato. Infatti questa birra artigianale rispecchia la forza, la resistenza e la bellezza di questi fieri animali alpini. Una birra intensa ed alcolica che unisce l''eleganza di una corpo leggero alla potenza degli aromi affumicati e dei luppoli.', 8.5, 120, 5.00, '', '', 333.68, 86),
(28, 4, 25, 'endorama_golconda.jpg', 'Golconda', 'Kölsch', 'Ispirata alle tedesche Kölsch.\r\n4,8% vol, chiara, dissetante, secca.\r\nCon una luppolatura decisa ma non invadente.', 4.8, 30, 5.00, '', '', 267.52, 76),
(29, 4, 3, 'endorama_vermillion.jpg', 'Vermillion', 'American IPA', 'Ispirata alle "American IPA".\r\n5,8% vol, dal colore ambrato.\r\nAmara, intensamente luppolata e dal gusto deciso. \r\nPer chi apprezza Birre di carattere.', 5.8, 64, 6.00, '', '', 677.04, 182),
(30, 4, 26, 'endorama_malombra.jpg', 'Malombra', 'Saison', 'Ispirata alle belghe Saison.\r\n5,9% vol, leggermente speziata.\r\nProdotta con luppolo Sorachi Ace, profumata e armoniosa.', 5.9, 34, 5.00, '', 'Sorachi Ace', 197.79, 57),
(31, 4, 27, 'endorama_milkyman.jpg', 'Milkyman', 'Milk Stout', 'Ispirata alle inglesi Milk Stout.\r\n4,2% vol, nera.\r\nBirra con sentori di caffè, cioccolato, e una dolce morbidezza conferita dall''aggiunta di lattosio.\r\nDisponibile nel periodo autunno-inverno.', 4.2, 22, 5.00, '', '', 488.70, 135),
(32, 4, 28, 'endorama_caliban.jpg', 'Caliban', 'Tripel', 'Ispirata alle belghe Tripel.\r\n8,5% vol, caratterizzata dagli aromi fruttati e speziati dal lavoro del lievito.\r\nInsidiosa nonostante il suo aspetto innocente.\r\nDisponibile a novembre-dicembre e maggio-giugno.\r\nProdotta in collaborazione con SpazioTerzoMondo.', 8.5, 36, 5.00, '', '', 430.80, 120),
(33, 4, 29, 'endorama_santa_lucia.jpg', 'Santa Lucia', 'Old Ale', 'Ispirata alle inglesi Old Ale.\r\n8,3% vol.\r\nBirra dove la luppolatura generosa è ben bilanciata dagli aromi dei malti utilizzati.\r\nDisponibile a novembre-dicembre.\r\nProdotta in collaborazione con La Compagnia del Luppolo.', 8.3, 70, 5.00, '', '', 151.00, 50),
(34, 5, 13, 'hopskin_summer_eve.jpg', 'Summer Eve', 'Golden Ale', 'Golden Ale di ispirazione nordamericana, una birra leggera nel corpo e nel grado alcolico, ma di carattere: fatta per essere bevuta in quantità.\r\nPerfetta per la calura estiva o per pensare e desiderare l''estate nelle altre stagioni, può anche essere l''ideale debutto per chi non ha mai bevuto birre artigianali.', 4.0, 0, 5.00, 'Pils, Maris Otter, Monaco, Frumento', 'Citra, E.K. Golding, Hersbrucker', 9379.94, 964),
(35, 5, 15, 'hopskin_american_sunset.jpg', 'American Sunset', 'American Pale Ale', 'American Pale Ale realizzata con una robusta luppolatura da amaro, ma senza dry hopping, lasciando così ampio spazio anche ad aromi e sapori dei malti. \r\nOttima da gustare al tramonto come aperitivo, è però un''amica versatile e disponibile che può accompagnare qualsiasi momento o occasione.', 5.0, 0, 5.00, 'Maris Otter, Cara', 'Madarina Bavaria, Cascade, Amarillo', 2183.44, 649),
(36, 5, 30, 'hopskin_black_porridge.jpg', 'Black Porridge', 'Oatmeal Stout', 'Una birra che riscopre la tradizione inglese, risalente a fine Ottocento, delle Stout arricchite con avena e perciò nutrienti e corroboranti.\r\nSe proprio non la volete bere a colazione, può accompagnare un pomeriggio autunnale, una serata e molti piatti, dolci compresi.', 5.1, 28, 6.00, 'Maris Otter, Fiocchi d''avena, Crystal, Chocolate, Black', 'Fuggle', 316.84, 89),
(37, 5, 3, 'hopskin_ipa.jpg', 'IPA', 'American Ipa', 'Tecnicamente si tratta di un''American India Pale Ale, dal momento che i luppoli utilizzati sono rigorosamente a stelle e strisce.\r\nSi ispira alle birre che hanno trainato la rinascita artigianale statunitense, che si è poi diffusa come una benefica pandemia in tutto il mondo.\r\nBirra di carattere che può accompagnare una lunga serata di bevute o il sedersi a tavola, e che può unirsi egregiamente con molti piatti.', 6.0, 0, 5.00, 'Maris Otter, Monaco, Cara', 'Columbus, Centennial, Simcoe, Citra', 513.22, 134),
(38, 5, 31, 'hopskin_tsunami.jpg', 'Tsunami', 'West Coast Pale Ale', 'Birra chiara, leggera, con una importante luppolatura in continuo. Forti le note di frutta tropicale.', 4.5, 25, 5.00, 'Pils', 'El Dorado, Mosaic, Hall. Blanc', 511.01, 137),
(39, 5, 33, 'hopskin_crazy_joy.jpg', 'Crazy Joy', 'American Saison', 'Birra chiara, secca, con un buon tenore alcolico. Incrocio delle speziature del lievito saison e delle note agrumate dei luppoli impiegati.', 7.2, 26, 5.00, 'Pils, Maltodestrine', 'Sorachi Ace, Amarillo', 293.25, 78),
(40, 5, 33, 'hopskin_crazy_paul.jpg', 'Crazy Paul', 'American Saison', 'Birra chiara, molto secca, con evidenti note speziate e fruttate e un finale leggermente citrico.', 6.5, 0, 5.00, 'Pils, Monaco, Frumento', 'Aramis, Citra', 267.18, 73),
(41, 5, 5, 'hopskin_lusty_cathy.jpg', 'Lusty Cathy', 'Imperial Stout', 'Imperial stout. Birra scura, cremosa, con forti sentori di cioccolato amaro e liquirizia.', 9.0, 0, 8.00, 'Maris Otter, Fiocchi d''orzo, Crystal, Roasted, Black, Chocolate', 'Chinook', 216.25, 57),
(42, 5, 32, 'hopskin_skittis.jpg', 'Skittis', 'Double Ipa', '', 8.0, 0, 5.00, 'Maris Otter, Monaco, Maltodestrine', 'Columbus, Chinook, Equinox, Centennial, Amarillo', 217.12, 59),
(43, 5, 34, 'hopskin_walters.jpg', 'Walter''s', 'White Ipa', '', 5.8, 0, 7.00, 'Pils, Frumento', 'Centennial, Cascade', 163.38, 45);

-- --------------------------------------------------------

--
-- Struttura della tabella `birra_luppolo`
--

CREATE TABLE IF NOT EXISTS `birra_luppolo` (
  `birra_id` int(11) NOT NULL,
  `luppolo_id` int(11) NOT NULL,
  PRIMARY KEY (`birra_id`,`luppolo_id`),
  KEY `luppolo_id` (`luppolo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `birra_luppolo`
--

INSERT INTO `birra_luppolo` (`birra_id`, `luppolo_id`) VALUES
(10, 1),
(3, 2),
(35, 2),
(39, 2),
(42, 2),
(40, 3),
(2, 4),
(3, 4),
(4, 4),
(8, 4),
(10, 4),
(35, 4),
(43, 4),
(2, 5),
(3, 5),
(37, 5),
(42, 5),
(43, 5),
(5, 6),
(6, 6),
(7, 6),
(41, 6),
(42, 6),
(2, 7),
(34, 7),
(37, 7),
(40, 7),
(2, 8),
(3, 8),
(4, 8),
(10, 8),
(37, 8),
(42, 8),
(34, 9),
(38, 10),
(42, 11),
(36, 12),
(38, 13),
(34, 14),
(35, 15),
(4, 16),
(6, 16),
(38, 16),
(4, 17),
(9, 17),
(2, 19),
(7, 19),
(37, 19),
(2, 20),
(11, 20),
(30, 20),
(39, 20),
(30, 21),
(39, 21),
(2, 22),
(6, 22);

-- --------------------------------------------------------

--
-- Struttura della tabella `birra_malto`
--

CREATE TABLE IF NOT EXISTS `birra_malto` (
  `birra_id` int(11) NOT NULL,
  `malto_id` int(11) NOT NULL,
  PRIMARY KEY (`birra_id`,`malto_id`),
  KEY `malto_id` (`malto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `birra_malto`
--

INSERT INTO `birra_malto` (`birra_id`, `malto_id`) VALUES
(5, 1),
(10, 1),
(36, 1),
(41, 1),
(35, 2),
(37, 2),
(5, 3),
(10, 3),
(36, 3),
(41, 3),
(2, 4),
(3, 4),
(4, 4),
(5, 4),
(6, 4),
(9, 4),
(10, 4),
(36, 4),
(41, 4),
(6, 5),
(9, 5),
(10, 5),
(36, 6),
(34, 7),
(40, 7),
(43, 7),
(39, 8),
(42, 8),
(34, 9),
(35, 9),
(36, 9),
(37, 9),
(41, 9),
(42, 9),
(9, 10),
(2, 11),
(4, 11),
(5, 11),
(6, 11),
(8, 11),
(9, 11),
(34, 11),
(37, 11),
(40, 11),
(42, 11),
(4, 12),
(5, 12),
(9, 12),
(2, 13),
(6, 13),
(8, 13),
(9, 13),
(1, 14),
(3, 14),
(4, 14),
(5, 14),
(6, 14),
(7, 14),
(8, 14),
(9, 14),
(10, 14),
(11, 14),
(9, 15),
(1, 16),
(2, 16),
(11, 16),
(12, 16),
(34, 16),
(38, 16),
(39, 16),
(40, 16),
(43, 16),
(5, 17),
(8, 17);

-- --------------------------------------------------------

--
-- Struttura della tabella `birrificio`
--

CREATE TABLE IF NOT EXISTS `birrificio` (
  `birrificio_id` int(11) NOT NULL AUTO_INCREMENT,
  `imageUrl` varchar(200) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `indirizzo` varchar(100) NOT NULL,
  `cap` varchar(50) NOT NULL,
  `comune` varchar(100) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `sito_web` varchar(100) NOT NULL,
  `anno_fondazione` int(11) DEFAULT NULL,
  `descrizione` varchar(4000) NOT NULL,
  PRIMARY KEY (`birrificio_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dump dei dati per la tabella `birrificio`
--

INSERT INTO `birrificio` (`birrificio_id`, `imageUrl`, `nome`, `indirizzo`, `cap`, `comune`, `provincia`, `sito_web`, `anno_fondazione`, `descrizione`) VALUES
(1, 'logo_elav.png', 'Birrificio Indip. Elav', 'Via Autieri d''Italia, 268', '24040', 'Comun Nuovo', 'BG', 'http://www.elavbrewery.com', 2010, ''),
(2, 'logo_valcavallina.png', 'Birrificio Valcavallina', 'Via del Tonale e della Mendola, 17/A', '24060', 'Endine Gaiano', 'BG', 'http://www.birrificiovalcavallina.com', 2009, ''),
(3, 'logo_viapriula.png', 'Via Priula', 'Via dei Partigiani, 10', '24016', 'San Pellegrino Terme', 'BG', 'http://www.birrificioviapriula.it', 2010, ''),
(4, 'logo_endorama.png', 'Birrificio Endorama', 'Via Boschetti, 97/6', '24050', 'Grassobbio', 'BG', 'http://www.endorama.it', NULL, ''),
(5, 'logo_hopskin.png', 'Hop Skin', 'Via Lega Lombarda, 15/17', '24035', 'Curno', 'BG', 'http://www.hopskin.it', NULL, '');

-- --------------------------------------------------------

--
-- Struttura della tabella `luppolo`
--

CREATE TABLE IF NOT EXISTS `luppolo` (
  `luppolo_id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `descrizione` text NOT NULL,
  PRIMARY KEY (`luppolo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dump dei dati per la tabella `luppolo`
--

INSERT INTO `luppolo` (`luppolo_id`, `nome`, `descrizione`) VALUES
(1, 'Ahtanum', ''),
(2, 'Amarillo', ''),
(3, 'Aramis', ''),
(4, 'Cascade', ''),
(5, 'Centennial', ''),
(6, 'Chinook', ''),
(7, 'Citra', ''),
(8, 'Columbus', ''),
(9, 'E.K. Golding', ''),
(10, 'El Dorado', ''),
(11, 'Equinox', ''),
(12, 'Fuggle', ''),
(13, 'Hall. Blanc', ''),
(14, 'Hersbrucker', ''),
(15, 'Madarina Bavaria', ''),
(16, 'Mosaic', ''),
(17, 'Palisade', ''),
(18, 'Saaz', ''),
(19, 'Simcoe', ''),
(20, 'Sorachi', ''),
(21, 'Sorachi Ace', ''),
(22, 'Willamette', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `malto`
--

CREATE TABLE IF NOT EXISTS `malto` (
  `malto_id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `descrizione` text NOT NULL,
  PRIMARY KEY (`malto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dump dei dati per la tabella `malto`
--

INSERT INTO `malto` (`malto_id`, `nome`, `descrizione`) VALUES
(1, 'Black', ''),
(2, 'Cara', ''),
(3, 'Chocolate', ''),
(4, 'Crystal', ''),
(5, 'Crystal Scuro', ''),
(6, 'Fiocchi d''avena', ''),
(7, 'Frumento', ''),
(8, 'Maltodestrine', ''),
(9, 'Maris Otter', ''),
(10, 'Melano', ''),
(11, 'Monaco', ''),
(12, 'Monaco Chiaro', ''),
(13, 'Monaco Scuro', ''),
(14, 'Pale', ''),
(15, 'Peated', ''),
(16, 'Pils', ''),
(17, 'Roasted Barley', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE IF NOT EXISTS `ordine` (
  `ordine_id` int(11) NOT NULL AUTO_INCREMENT,
  `utente_id` int(11) NOT NULL,
  `punto_vendita_id` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `stato` varchar(50) NOT NULL,
  PRIMARY KEY (`ordine_id`),
  KEY `utente_id` (`utente_id`),
  KEY `punto_vendita_id` (`punto_vendita_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`ordine_id`, `utente_id`, `punto_vendita_id`, `data`, `stato`) VALUES
(1, 1, 1, '2016-03-01 00:00:00', 'acquistato'),
(2, 1, 1, '2016-03-02 00:00:00', 'carrello'),
(3, 1, 2, '2016-03-03 00:00:00', 'acquistato'),
(4, 2, 2, '2016-03-01 00:00:00', 'acquistato'),
(5, 2, 1, '2016-03-02 00:00:00', 'acquistato'),
(6, 3, 1, '2016-03-15 00:00:00', 'acquistato'),
(7, 3, 1, '2016-03-16 00:00:00', 'carrello'),
(8, 3, 1, '2016-03-17 00:00:00', 'acquistato'),
(9, 3, 1, '2016-03-18 00:00:00', 'acquistato'),
(10, 3, 1, '2016-03-19 00:00:00', 'acquistato');

-- --------------------------------------------------------

--
-- Struttura della tabella `punto_vendita`
--

CREATE TABLE IF NOT EXISTS `punto_vendita` (
  `punto_vendita_id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  PRIMARY KEY (`punto_vendita_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `punto_vendita`
--

INSERT INTO `punto_vendita` (`punto_vendita_id`, `nome`) VALUES
(1, 'sito web'),
(2, 'negozio a Bergamo');

-- --------------------------------------------------------

--
-- Struttura della tabella `riga_ordine`
--

CREATE TABLE IF NOT EXISTS `riga_ordine` (
  `ordine_id` int(11) NOT NULL,
  `birra_id` int(11) NOT NULL,
  `descrizione` varchar(500) NOT NULL,
  `quantita` int(11) NOT NULL,
  `importo_unitario` decimal(10,2) NOT NULL,
  `posizione` smallint(6) NOT NULL,
  PRIMARY KEY (`ordine_id`,`birra_id`),
  KEY `birra_id` (`birra_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `riga_ordine`
--

INSERT INTO `riga_ordine` (`ordine_id`, `birra_id`, `descrizione`, `quantita`, `importo_unitario`, `posizione`) VALUES
(1, 5, 'Birrificio Indip. Elav - Dark Metal', 2, 6.00, 1),
(1, 24, 'Via Priula - Dubec', 2, 5.00, 3),
(1, 29, 'Birrificio Endorama - Vermillion', 5, 6.00, 2),
(2, 6, 'Birrificio Indip. Elav - Uppercut IPA', 2, 5.00, 2),
(2, 14, 'Birrificio Valcavallina - Polly Jean', 4, 5.00, 1),
(2, 36, 'Hop Skin - Black Porridge', 5, 6.00, 3),
(3, 6, 'Birrificio Indip. Elav - Uppercut IPA', 5, 5.00, 1),
(3, 10, 'Birrificio Indip. Elav - Regina d''Inverno', 2, 5.00, 2),
(3, 20, 'Via Priula - Loertìs', 2, 5.00, 3),
(3, 41, 'Hop Skin - Lusty Cathy', 8, 8.00, 4),
(4, 2, 'Birrificio Indip. Elav - Indie Ale', 4, 5.00, 2),
(4, 14, 'Birrificio Valcavallina - Polly Jean', 2, 5.00, 1),
(5, 14, 'Birrificio Valcavallina - Polly Jean', 5, 5.00, 3),
(5, 19, 'Birrificio Valcavallina - Diavolo', 2, 5.00, 1),
(5, 23, 'Via Priula - Corna Bianca', 2, 5.00, 2),
(6, 6, 'Birrificio Indip. Elav - Uppercut IPA', 8, 5.00, 1),
(6, 25, 'Via Priula - Morosa', 2, 4.00, 3),
(6, 27, 'Via Priula - Camoz', 2, 5.00, 2),
(7, 3, 'Birrificio Indip. Elav - Grunge IPA', 5, 5.00, 1),
(7, 25, 'Via Priula - Morosa', 5, 4.00, 4),
(7, 27, 'Via Priula - Camoz', 2, 5.00, 3),
(7, 28, 'Birrificio Endorama - Golconda', 2, 5.00, 2),
(7, 31, 'Birrificio Endorama - Milkyman', 8, 5.00, 5),
(8, 6, 'Birrificio Indip. Elav - Uppercut IPA', 4, 5.00, 1),
(8, 31, 'Birrificio Endorama - Milkyman', 5, 5.00, 2),
(9, 13, 'Birrificio Valcavallina - Sunflower', 4, 5.00, 1),
(9, 23, 'Via Priula - Corna Bianca', 5, 5.00, 3),
(9, 34, 'Hop Skin - Summer Eve', 4, 5.00, 2),
(9, 40, 'Hop Skin - Crazy Paul', 5, 5.00, 4),
(10, 10, 'Birrificio Indip. Elav - Regina d''Inverno', 4, 5.00, 1),
(10, 41, 'Hop Skin - Lusty Cathy', 6, 8.00, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `stile`
--

CREATE TABLE IF NOT EXISTS `stile` (
  `stile_id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `descrizione` text NOT NULL,
  PRIMARY KEY (`stile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dump dei dati per la tabella `stile`
--

INSERT INTO `stile` (`stile_id`, `nome`, `descrizione`) VALUES
(1, 'Pale Ale', ''),
(2, 'Amber Ale', ''),
(3, 'American Ipa', ''),
(4, 'Best Bitter', ''),
(5, 'Imperial Stout', ''),
(6, 'Ipa', ''),
(7, 'Imperial Ipa', ''),
(8, 'Black Ipa', ''),
(9, 'Scotch Ale', ''),
(10, 'Porter', ''),
(11, 'Barley Wine', ''),
(12, 'Blonde Ale', ''),
(13, 'Golden Ale', ''),
(14, 'Light IPA', ''),
(15, 'American Pale Ale', ''),
(16, 'Brown Ale', ''),
(17, 'Extra Special Bitter', ''),
(18, 'Smoked Porter', ''),
(19, 'Bohemian Pils', ''),
(20, 'Speciale alla frutta (lamponi)', ''),
(21, 'Blanche', ''),
(22, 'Doppelbock', ''),
(23, 'Dubbel alla frutta (more)', ''),
(24, 'Tripel con "Achillea Millefoglie"', ''),
(25, 'Kölsch', ''),
(26, 'Saison', ''),
(27, 'Milk Stout', ''),
(28, 'Tripel', ''),
(29, 'Old Ale', ''),
(30, 'Oatmeal Stout', ''),
(31, 'West Coast Pale Ale', ''),
(32, 'Double Ipa', ''),
(33, 'American Saison', ''),
(34, 'White Ipa', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE IF NOT EXISTS `utente` (
  `utente_id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `cognome` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `indirizzo` varchar(500) NOT NULL,
  `cap` varchar(50) NOT NULL,
  `comune` varchar(200) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `data_registrazione` datetime NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`utente_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`utente_id`, `nome`, `cognome`, `email`, `indirizzo`, `cap`, `comune`, `provincia`, `data_registrazione`, `password`) VALUES
(1, 'Mario', 'Rossi', 'mario@gmail.com', '', '', '', '', '2016-03-01 00:00:00', 'marione'),
(2, 'Luigi', 'Bianchi', 'luigi@gmail.com', '', '', '', '', '2016-03-02 00:00:00', 'luigino'),
(3, 'Gianno', 'Verdi', 'giacomo@gmail.com', '', '', '', '', '2016-03-10 00:00:00', 'lollolo'),
(4, 'Marco', 'Locatelli', 'marco@gmail.com', '', '', '', '', '2016-03-15 00:00:00', 'marcolocat');

-- --------------------------------------------------------

--
-- Struttura della tabella `voto`
--

CREATE TABLE IF NOT EXISTS `voto` (
  `utente_id` int(11) NOT NULL,
  `birra_id` int(11) NOT NULL,
  `voto` smallint(6) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`utente_id`,`birra_id`),
  KEY `birra_id` (`birra_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `voto`
--

INSERT INTO `voto` (`utente_id`, `birra_id`, `voto`, `data`) VALUES
(1, 1, 8, '2016-03-02 00:00:00'),
(1, 2, 7, '2016-03-03 00:00:00'),
(1, 3, 9, '2016-03-09 00:00:00'),
(1, 4, 7, '2016-03-10 00:00:00'),
(1, 5, 9, '2016-03-10 00:00:00'),
(1, 6, 5, '2016-03-10 00:00:00'),
(1, 9, 4, '2016-03-09 00:00:00'),
(1, 10, 9, '2016-03-10 00:00:00'),
(1, 11, 6, '2016-03-10 00:00:00'),
(1, 12, 7, '2016-03-10 00:00:00'),
(1, 15, 9, '2016-03-10 00:00:00'),
(1, 16, 5, '2016-03-10 00:00:00'),
(1, 19, 4, '2016-03-09 00:00:00'),
(1, 20, 9, '2016-03-10 00:00:00'),
(1, 21, 6, '2016-03-10 00:00:00'),
(1, 22, 7, '2016-03-10 00:00:00'),
(1, 23, 9, '2016-03-09 00:00:00'),
(1, 24, 7, '2016-03-10 00:00:00'),
(1, 25, 9, '2016-03-10 00:00:00'),
(1, 26, 5, '2016-03-10 00:00:00'),
(1, 29, 4, '2016-03-09 00:00:00'),
(1, 33, 9, '2016-03-09 00:00:00'),
(1, 34, 7, '2016-03-10 00:00:00'),
(1, 35, 9, '2016-03-10 00:00:00'),
(1, 36, 5, '2016-03-10 00:00:00'),
(1, 37, 8, '2016-03-02 00:00:00'),
(1, 38, 7, '2016-03-03 00:00:00'),
(1, 39, 4, '2016-03-09 00:00:00'),
(1, 40, 9, '2016-03-10 00:00:00'),
(1, 41, 6, '2016-03-10 00:00:00'),
(1, 42, 7, '2016-03-10 00:00:00'),
(1, 43, 9, '2016-03-09 00:00:00'),
(2, 1, 5, '2016-03-05 00:00:00'),
(2, 2, 6, '2016-03-03 00:00:00'),
(2, 3, 3, '2016-03-09 00:00:00'),
(2, 4, 7, '2016-03-19 00:00:00'),
(2, 7, 9, '2016-03-05 00:00:00'),
(2, 8, 4, '2016-03-03 00:00:00'),
(2, 9, 5, '2016-03-09 00:00:00'),
(2, 10, 6, '2016-03-19 00:00:00'),
(2, 11, 5, '2016-03-19 00:00:00'),
(2, 12, 8, '2016-03-19 00:00:00'),
(2, 13, 7, '2016-03-09 00:00:00'),
(2, 17, 6, '2016-03-05 00:00:00'),
(2, 18, 7, '2016-03-03 00:00:00'),
(2, 19, 8, '2016-03-09 00:00:00'),
(2, 20, 4, '2016-03-19 00:00:00'),
(2, 24, 6, '2016-03-19 00:00:00'),
(2, 25, 7, '2016-03-19 00:00:00'),
(2, 27, 6, '2016-03-05 00:00:00'),
(2, 28, 5, '2016-03-03 00:00:00'),
(2, 29, 5, '2016-03-09 00:00:00'),
(2, 30, 7, '2016-03-19 00:00:00'),
(2, 31, 8, '2016-03-19 00:00:00'),
(2, 32, 3, '2016-03-19 00:00:00'),
(2, 33, 8, '2016-03-09 00:00:00'),
(2, 34, 6, '2016-03-19 00:00:00'),
(2, 35, 4, '2016-03-19 00:00:00'),
(2, 38, 6, '2016-03-03 00:00:00'),
(2, 39, 5, '2016-03-09 00:00:00'),
(2, 40, 7, '2016-03-19 00:00:00'),
(2, 41, 8, '2016-03-19 00:00:00'),
(2, 42, 9, '2016-03-19 00:00:00'),
(2, 43, 6, '2016-03-09 00:00:00'),
(3, 1, 6, '2016-03-02 00:00:00'),
(3, 2, 7, '2016-03-03 00:00:00'),
(3, 3, 6, '2016-03-09 00:00:00'),
(3, 4, 7, '2016-03-10 00:00:00'),
(3, 6, 5, '2016-03-10 00:00:00'),
(3, 7, 6, '2016-03-02 00:00:00'),
(3, 8, 7, '2016-03-03 00:00:00'),
(3, 9, 4, '2016-03-09 00:00:00'),
(3, 10, 6, '2016-03-16 00:00:00'),
(3, 11, 6, '2016-03-16 00:00:00'),
(3, 12, 7, '2016-03-16 00:00:00'),
(3, 15, 8, '2016-03-16 00:00:00'),
(3, 16, 7, '2016-03-16 00:00:00'),
(3, 17, 6, '2016-03-02 00:00:00'),
(3, 18, 7, '2016-03-03 00:00:00'),
(3, 19, 4, '2016-03-09 00:00:00'),
(3, 20, 5, '2016-03-16 00:00:00'),
(3, 21, 6, '2016-03-16 00:00:00'),
(3, 22, 7, '2016-03-16 00:00:00'),
(3, 23, 8, '2016-03-09 00:00:00'),
(3, 24, 7, '2016-03-16 00:00:00'),
(3, 25, 8, '2016-03-10 00:00:00'),
(3, 26, 5, '2016-03-10 00:00:00'),
(3, 27, 6, '2016-03-02 00:00:00'),
(3, 28, 7, '2016-03-03 00:00:00'),
(3, 29, 4, '2016-03-09 00:00:00'),
(3, 30, 8, '2016-03-10 00:00:00'),
(3, 31, 6, '2016-03-10 00:00:00'),
(3, 32, 7, '2016-03-10 00:00:00'),
(3, 33, 8, '2016-03-09 00:00:00'),
(3, 34, 7, '2016-03-10 00:00:00'),
(3, 35, 8, '2016-03-10 00:00:00'),
(3, 36, 5, '2016-03-10 00:00:00'),
(3, 37, 7, '2016-03-02 00:00:00'),
(3, 38, 7, '2016-03-03 00:00:00'),
(3, 39, 4, '2016-03-09 00:00:00'),
(3, 40, 5, '2016-03-10 00:00:00'),
(3, 42, 7, '2016-03-10 00:00:00'),
(3, 43, 8, '2016-03-09 00:00:00'),
(4, 1, 5, '2016-03-05 00:00:00'),
(4, 2, 8, '2016-03-03 00:00:00'),
(4, 3, 3, '2016-03-09 00:00:00'),
(4, 4, 7, '2016-03-19 00:00:00'),
(4, 5, 6, '2016-03-19 00:00:00'),
(4, 6, 6, '2016-03-19 00:00:00'),
(4, 7, 8, '2016-03-05 00:00:00'),
(4, 8, 4, '2016-03-03 00:00:00'),
(4, 9, 5, '2016-03-09 00:00:00'),
(4, 10, 6, '2016-03-19 00:00:00'),
(4, 11, 5, '2016-03-19 00:00:00'),
(4, 12, 6, '2016-03-19 00:00:00'),
(4, 13, 7, '2016-03-09 00:00:00'),
(4, 14, 6, '2016-03-19 00:00:00'),
(4, 15, 5, '2016-03-19 00:00:00'),
(4, 18, 7, '2016-03-03 00:00:00'),
(4, 19, 6, '2016-03-09 00:00:00'),
(4, 20, 4, '2016-03-19 00:00:00'),
(4, 21, 5, '2016-03-19 00:00:00'),
(4, 22, 6, '2016-03-19 00:00:00'),
(4, 23, 4, '2016-03-09 00:00:00'),
(4, 24, 6, '2016-03-19 00:00:00'),
(4, 26, 5, '2016-03-19 00:00:00'),
(4, 27, 6, '2016-03-05 00:00:00'),
(4, 28, 5, '2016-03-03 00:00:00'),
(4, 29, 5, '2016-03-09 00:00:00'),
(4, 30, 7, '2016-03-19 00:00:00'),
(4, 31, 6, '2016-03-19 00:00:00'),
(4, 35, 5, '2016-03-19 00:00:00'),
(4, 37, 6, '2016-03-05 00:00:00'),
(4, 38, 6, '2016-03-03 00:00:00'),
(4, 39, 5, '2016-03-09 00:00:00'),
(4, 40, 7, '2016-03-19 00:00:00'),
(4, 41, 6, '2016-03-19 00:00:00'),
(4, 42, 8, '2016-03-19 00:00:00'),
(4, 43, 6, '2016-03-09 00:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `wishlist`
--

CREATE TABLE IF NOT EXISTS `wishlist` (
  `utente_id` int(11) NOT NULL,
  `birra_id` int(11) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`utente_id`,`birra_id`),
  KEY `birra_id` (`birra_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `wishlist`
--

INSERT INTO `wishlist` (`utente_id`, `birra_id`, `data`) VALUES
(1, 5, '2016-02-02 00:00:00'),
(1, 27, '2016-02-10 00:00:00'),
(1, 41, '2016-03-02 00:00:00'),
(3, 5, '2016-03-01 00:00:00'),
(3, 7, '2016-03-02 00:00:00'),
(3, 11, '2016-03-02 00:00:00'),
(3, 40, '2016-03-02 00:00:00'),
(3, 41, '2016-03-02 00:00:00'),
(3, 42, '2016-03-04 00:00:00'),
(4, 4, '2016-03-05 00:00:00'),
(4, 20, '2016-03-13 00:00:00'),
(4, 35, '2016-03-15 00:00:00');

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `birra`
--
ALTER TABLE `birra`
  ADD CONSTRAINT `birra_ibfk_1` FOREIGN KEY (`birrificio_id`) REFERENCES `birrificio` (`birrificio_id`),
  ADD CONSTRAINT `birra_ibfk_2` FOREIGN KEY (`stile_id`) REFERENCES `stile` (`stile_id`) ON DELETE SET NULL;

--
-- Limiti per la tabella `birra_luppolo`
--
ALTER TABLE `birra_luppolo`
  ADD CONSTRAINT `birra_luppolo_ibfk_2` FOREIGN KEY (`luppolo_id`) REFERENCES `luppolo` (`luppolo_id`),
  ADD CONSTRAINT `birra_luppolo_ibfk_1` FOREIGN KEY (`birra_id`) REFERENCES `birra` (`birra_id`);

--
-- Limiti per la tabella `birra_malto`
--
ALTER TABLE `birra_malto`
  ADD CONSTRAINT `birra_malto_ibfk_2` FOREIGN KEY (`malto_id`) REFERENCES `malto` (`malto_id`),
  ADD CONSTRAINT `birra_malto_ibfk_1` FOREIGN KEY (`birra_id`) REFERENCES `birra` (`birra_id`);

--
-- Limiti per la tabella `ordine`
--
ALTER TABLE `ordine`
  ADD CONSTRAINT `ordine_ibfk_2` FOREIGN KEY (`punto_vendita_id`) REFERENCES `punto_vendita` (`punto_vendita_id`),
  ADD CONSTRAINT `ordine_ibfk_1` FOREIGN KEY (`utente_id`) REFERENCES `utente` (`utente_id`);

--
-- Limiti per la tabella `riga_ordine`
--
ALTER TABLE `riga_ordine`
  ADD CONSTRAINT `riga_ordine_ibfk_1` FOREIGN KEY (`ordine_id`) REFERENCES `ordine` (`ordine_id`),
  ADD CONSTRAINT `riga_ordine_ibfk_2` FOREIGN KEY (`birra_id`) REFERENCES `birra` (`birra_id`);

--
-- Limiti per la tabella `voto`
--
ALTER TABLE `voto`
  ADD CONSTRAINT `voto_ibfk_2` FOREIGN KEY (`birra_id`) REFERENCES `birra` (`birra_id`),
  ADD CONSTRAINT `voto_ibfk_1` FOREIGN KEY (`utente_id`) REFERENCES `utente` (`utente_id`);

--
-- Limiti per la tabella `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_ibfk_2` FOREIGN KEY (`birra_id`) REFERENCES `birra` (`birra_id`),
  ADD CONSTRAINT `wishlist_ibfk_1` FOREIGN KEY (`utente_id`) REFERENCES `utente` (`utente_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
